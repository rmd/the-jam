# The Jam
A new song structure created every day.

## URLs
http://robsdreamco.com/jam-with-us - a new song every day
http://robsdreamco.com/jam-with-us/seed - a random song any time
http://robsdreamco.com/jam-with-us/seed/[seed] - the same song every time, eventually

**Note** The algorithm is not stable, and every time it changes the song that is generated from any given seed is going to be different. So if you like any particular structure, make a copy of it!

## About
So the idea is that

There's a todo list in `./docs/project.md`. I make no promise

## Contributing
This is pretty new and this is a section being included optimistically. There are no conventions at this time. Open an issue or a pull request if you feel motivated to do so and we can figure it out.

## License
MIT.

See `./LICENSE`