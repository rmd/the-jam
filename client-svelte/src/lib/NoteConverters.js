const sharpKeys = [
    'a', 'a#', 'b', 'c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#'
]

const flatKeys = [
    'a', 'bb', 'b', 'c', 'db', 'd', 'eb', 'e', 'f', 'gb', 'g', 'ab' 
]   

const semitonesAway = []
semitonesAway['I'] = 0
semitonesAway['♭II'] = 1
semitonesAway['II'] = 2
semitonesAway['♭III'] = 3
semitonesAway['III'] = 4
semitonesAway['IV'] = 5
semitonesAway['V'] = 7
semitonesAway['♭VI'] = 8
semitonesAway['VI'] = 9
semitonesAway['♭VII'] = 10

semitonesAway['VII'] = 11
semitonesAway['VIII'] = 12
semitonesAway['i'] = 0
semitonesAway['ii'] = 2
semitonesAway['iii'] = 3
semitonesAway['iv'] = 5
semitonesAway['v'] = 7
semitonesAway['vi'] = 8
semitonesAway['vii'] = 10
semitonesAway['viii'] = 12

export default class NoteConverters {
    // RMD TODO this whole thing is begging for a refactor
    static noteFromKeySignatureAndNumeral = (keySignature, numeral) => {
        let trueRoot = (keySignature.indexOf('m') === -1) 
            ? keySignature
            : keySignature.substring(0, keySignature.indexOf('m'))

            // This is a hack. At some point there will need to be a better way of determining the root instead of 
        // hardcoding the sharps and flats arrays
        if (trueRoot === 'Cb') trueRoot = 'B'

        trueRoot = trueRoot.toLowerCase()

        const semitonesToAdd = semitonesAway[numeral]
        if(sharpKeys.indexOf(trueRoot) !== -1) {
            const rootIdx = sharpKeys.indexOf(trueRoot)
            let noteIdx = rootIdx + semitonesToAdd
            if(noteIdx >= sharpKeys.length) noteIdx -= sharpKeys.length

            // console.log("SK NI", sharpKeys, noteIdx, trueRoot, rootIdx, semitonesToAdd)
            return sharpKeys[noteIdx].toLowerCase()
        }

        if(flatKeys.indexOf(trueRoot) !== -1) {
            const rootIdx = flatKeys.indexOf(trueRoot)
            let noteIdx = rootIdx + semitonesToAdd
            if(noteIdx >= flatKeys.length) noteIdx -= flatKeys.length

            return flatKeys[noteIdx].toLowerCase()
        }

        console.error("COULD NOT FIND ROOT NOTE", keySignature, numeral, trueRoot)
    }

    static addNumeralToNote(note, numeral) {
        const semitonesToAdd = semitonesAway[numeral]  

        const needle = note.toLowerCase()

        if(sharpKeys.indexOf(needle) !== -1) {
            const rootIdx = sharpKeys.indexOf(needle)
            let noteIdx = rootIdx + semitonesToAdd
            if(noteIdx >= sharpKeys.length) noteIdx -= sharpKeys.length

            return sharpKeys[noteIdx].toLowerCase()
        }

        if(flatKeys.indexOf(needle) !== -1) {
            const rootIdx = flatKeys.indexOf(needle)
            let noteIdx = rootIdx + semitonesToAdd
            if(noteIdx >= flatKeys.length) noteIdx -= flatKeys.length

            return flatKeys[noteIdx].toLowerCase()
        }

        console.error("COULD NOT FIND NOTE", note, needle)
    }
}