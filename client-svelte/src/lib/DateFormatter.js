export default class DateFormatter {
    static format = (date) => {
        const days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ]

        const months = [
            'January',
            'February',
            'March', 
            'April',
            'May', 
            'June', 
            'July',
            'August',
            'September',
            'October', 
            'November', 
            'December',
        ]

        // Saturday, October 14, 2023 UTC
        return `${days[date.getUTCDay()]}, ${months[date.getUTCMonth()]} ${date.getUTCDate()}, ${date.getUTCFullYear()} UTC`
    }
}