import NoteConverters from "./NoteConverters"

export default class Section {
    static TYPE_INTRO = 'TYPE_INTRO'
    static TYPE_VERSE = 'TYPE_VERSE'
    static TYPE_PRECHORUS = 'TYPE_PRECHORUS'
    static TYPE_CHORUS = 'TYPE_CHORUS'
    static TYPE_POSTCHORUS = 'TYPE_POSTCHORUS'
    static TYPE_BRIDGE = 'TYPE_BRIDGE'
    static TYPE_OUTRO = 'TYPE_OUTRO'

    constructor(
        barCount,
        bpm,
        keySignature,
        progression,
        tempo,
        timeSignature,
        type
    ) {
        this.barCount = barCount
        this.bpm = bpm
        this.keySignature = keySignature
        this.progression = progression
        this.tempo = tempo
        this.timeSignature = timeSignature
        this.type = type
        
        this.progressions = []
        for(let barIndex = 0; barIndex < barCount; barIndex++) {
            this.progressions = [...this.progressions, ...progression.numerals.split('-')]
        }

        this.notes = []
        this.progressions.forEach( progression => {
            return this.notes.push(
                NoteConverters.noteFromKeySignatureAndNumeral(this.keySignature.pitches[0], progression)
            )
        })
    }
}
