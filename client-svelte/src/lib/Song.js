import Prando from 'prando'

import keySignatures from '$lib/data/key-signatures.json'
import progressions from '$lib/data/progressions.json'
import songStructures from '$lib/data/song-structures.json'
import tempos from '$lib/data/tempos.json'
import timeSignatures from '$lib/data/time-signatures.json'

import Section from '$lib/Section'

export default class Song {
    constructor(seed) {
        // Default to timestamp as the seed
        seed = seed ? seed : Date.now()
        console.log(`Constructing song with seed`, seed)
        
        this.seed = seed
        this.rng = new Prando(this.seed)

        this.date = new Date()
        this.keySignature = this.pickKeySignature()

        console.log("this.keySig", this.keySignature)

        
        // this is the first time I've ever had a mandatory semi-colon that I'm aware of. 
        // I guess the array assignment messes something up here? without it, this.progression
        // gets the array containing tempo and bpm assigned to it. 
        this.progression = this.pickProgression();
        [this.tempo, this.bpm] = this.pickTempoAndBpm()
        
        this.timeSignature = this.pickTimeSignature()
        
        this.structure = this.pickStructure()
        
        
        this.generateSectionsAndStructure()

        console.log("Song constructed!", this)
    }

    generateSectionsAndStructure() {
        const sections = []

        let fullStructure = ''
        if(this.shouldHaveIntro()) {
            sections.push(this.makeIntro())
            fullStructure = `${fullStructure}I`
        }

        let verse, chorus, bridge
        Array.from(this.structure).map( letter => {
            // Verse
            if (letter === 'A') {
                if(!verse) {
                    verse = this.makeVerse()
                }

                sections.push(verse)
                fullStructure = `${fullStructure}${letter}`
            }

            // Chorus
            if (letter === 'B') {
                if(this.shouldHavePreChorus()) {
                    sections.push(this.makePreChorus())
                    fullStructure = `${fullStructure}X`
                } 

                if(!chorus) {
                    chorus = this.makeChorus()
                }
                sections.push(chorus)
                fullStructure = `${fullStructure}${letter}`

                if(this.shouldHavePostChorus()) {
                    sections.push(this.makePostChorus())
                    fullStructure = `${fullStructure}Y`
                }
            }

            // Bridge
            if (letter === 'C') {
                if(!bridge) {
                    bridge = this.makeBridge()
                }

                sections.push(bridge)
                fullStructure = `${fullStructure}${letter}`
            }
        })

        if(this.shouldHaveOutro()) {
            sections.push(this.makeOutro())
            fullStructure = `${fullStructure}O`
        }
        
        this.sections = sections
        this.structure = fullStructure
    }

    makeIntro() {
        if(!this.intro) {
            let bars = this.rng.nextArrayItem([1,1,1,1,2])
            let bpm = this.bpm
            let keySignature = this.keySignature
            let progression = this.progression
            let tempo = this.tempo
            let timeSignature = this.timeSignature

            // Rarely change key for intros
            if(this.percent() < 5) {
                keySignature = this.pickKeySignature()
            }

            // Usually change progression for intros
            if(this.percent() < 85) {
                progression = this.pickProgression()
            }

            // Very rarely change tempo and bpm for intros
            if(this.percent() < 1) {
                [tempo, bpm] = this.pickTempoAndBpm()
            }

            // Rarely change time signature for intros
            if(this.percent() < 10) {
                timeSignature = this.pickTimeSignature()
            }

            this.intro = new Section(
                bars,
                bpm,
                keySignature,
                progression,
                tempo,
                timeSignature,
                Section.TYPE_INTRO,
            )
        }

        return this.intro
    }

    makeVerse() {
        if(!this.verse) {
            let bars = this.rng.nextArrayItem([1,2,2,2,2,3])
            let bpm = this.bpm
            let keySignature = this.keySignature
            let progression = this.progression
            let tempo = this.tempo
            let timeSignature = this.timeSignature

            // Usually change key for verses
            if(this.percent() < 85) {
                keySignature = this.pickKeySignature()
            }

            // Usually change progression for verses
            if(this.percent() < 85) {
                progression = this.pickProgression()
            }

            // Very rarely change tempo and bpm for verses
            if(this.percent() < 1) {
                [tempo, bpm] = this.pickTempoAndBpm()
            }

            // Rarely change time signature for verses
            if(this.percent() < 5) {
                timeSignature = this.pickTimeSignature()
            }

            this.verse = new Section(
                bars,
                bpm,
                keySignature,
                progression,
                tempo,
                timeSignature,
                Section.TYPE_VERSE,
            )
        }

        return this.verse
    }

    makePreChorus() {
        if(!this.prechorus) {
            let bars = this.rng.nextArrayItem([1,1,1,1,2])
            let bpm = this.bpm
            let keySignature = this.keySignature
            let progression = this.progression
            let tempo = this.tempo
            let timeSignature = this.timeSignature

            // Rarely change key for pre-choruses
            if(this.percent() < 15) {
                keySignature = this.pickKeySignature()
            }

            // Rarely change progression for pre-choruses
            if(this.percent() < 15) {
                progression = this.pickProgression()
            }

            // Very rarely change tempo and bpm for pre-choruses
            if(this.percent() < 1) {
                [tempo, bpm] = this.pickTempoAndBpm()
            }

            // Rarely change time signature for pre-choruses
            if(this.percent() < 5) {
                timeSignature = this.pickTimeSignature()
            }

            this.prechorus = new Section(
                bars,
                bpm,
                keySignature,
                progression,
                tempo,
                timeSignature,
                Section.TYPE_PRECHORUS,
            )
        }

        return this.prechorus
    }

    makeChorus() {
        if(!this.chorus) {
            // The chorus and the song always have the same properties
            // Alternatively stated, the chorus' properties define the song's properties
            // I'm just doing it backward right now
            this.chorus = new Section(
                this.rng.nextArrayItem([1,2,2,2,2,3]),
                this.bpm,
                this.keySignature,
                this.progression,
                this.tempo,
                this.timeSignature,
                Section.TYPE_CHORUS,
            )
        }

        return this.chorus
    }

    makePostChorus() {
        if(!this.postchorus) {  
            let bars = this.rng.nextArrayItem([1,1,1,1,2])
            let bpm = this.bpm
            let keySignature = this.keySignature
            let progression = this.progression
            let tempo = this.tempo
            let timeSignature = this.timeSignature

            // Rarely change key for post-choruses
            if(this.percent() < 15) {
                keySignature = this.pickKeySignature()
            }

            // Occasionally change progression for post-choruses
            if(this.percent() < 25) {
                progression = this.pickProgression()
            }

            // Very rarely change tempo and bpm for post-choruses
            if(this.percent() < 1) {
                [tempo, bpm] = this.pickTempoAndBpm()
            }

            // Rarely change time signature for post-choruses
            if(this.percent() < 10) {
                timeSignature = this.pickTimeSignature()
            }

            
            this.postchorus = new Section(
                bars,
                bpm,
                keySignature,
                progression,
                tempo,
                timeSignature,
                Section.TYPE_POSTCHORUS,
            )
        }

        return this.postchorus
    }

    makeBridge() {
        if(!this.bridge) {
            let bars = this.rng.nextArrayItem([1,2,2,2,3,3])
            let bpm = this.bpm
            let keySignature = this.keySignature
            let progression = this.progression
            let tempo = this.tempo
            let timeSignature = this.timeSignature

            // Usually change key for bridges
            if(this.percent() < 85) {
                keySignature = this.pickKeySignature()
            }

            // Usually change progression for bridges
            if(this.percent() < 85) {
                progression = this.pickProgression()
            }

            // Very rarely change tempo and bpm for bridges
            if(this.percent() < 2) {
                [tempo, bpm] = this.pickTempoAndBpm()
            }

            // Rarely change time signature for bridges
            if(this.percent() < 5) {
                timeSignature = this.pickTimeSignature()
            }

            this.bridge = new Section(
                bars,
                bpm,
                keySignature,
                progression,
                tempo,
                timeSignature,
                Section.TYPE_BRIDGE,
            )
        }

        return this.bridge
    }

    makeOutro() {
        if(!this.outro) {
            let bars = this.rng.nextArrayItem([1,1,1,1,2])
            let bpm = this.bpm
            let keySignature = this.keySignature
            let progression = this.progression
            let tempo = this.tempo
            let timeSignature = this.timeSignature

            // Rarely change key for outros
            if(this.percent() < 15) {
                keySignature = this.pickKeySignature()
            }

            // Usually change progression for outros
            if(this.percent() < 85) {
                progression = this.pickProgression()
            }

            // Very rarely change tempo and bpm for outros
            if(this.percent() < 1) {
                [tempo, bpm] = this.pickTempoAndBpm()
            }

            // Rarely change time signature for outros
            if(this.percent() < 10) {
                timeSignature = this.pickTimeSignature()
            }

            this.outro = new Section(
                bars,
                bpm,
                keySignature,
                progression,
                tempo,
                timeSignature,
                Section.TYPE_OUTRO,
            )
        }

        return this.outro
    }

    pickKeySignature() { return this.weighAndPick(keySignatures.keySignatures) }
    pickTimeSignature() { return this.weighAndPick(timeSignatures) }
    pickTempoAndBpm() {
        const tempo = this.weighAndPick(tempos.tempos)
        const bpm = this.rng.nextInt(tempo.min, tempo.max)

        return [tempo, bpm]
    }
    pickProgression() { 
        return this.weighAndPick(progressions.progressions) 
    }
    pickStructure() { return this.weighAndPick(songStructures.structures).structure }

    shouldHaveIntro() {
        return this.rng.nextArrayItem([true, false, false, false, false, false, false])
    }

    shouldHaveOutro() {
        return this.rng.nextArrayItem([true, false, false, false, false, false, false])
    }

    shouldHavePreChorus() {
        return this.rng.nextArrayItem([true, false, false, false, false, false, false])
    }

    shouldHavePostChorus() {
        return this.rng.nextArrayItem([true, false, false, false, false, false, false])
    }

    percent() {
        return this.rng.nextInt(0, 100)
    }

    weighAndPick(source) {
        let haystack = []
        
        source.forEach(element => {
            for(let x = 0; x < element.weight; x++) {
                haystack.push(element)
            }
        });
        
        return this.rng.nextArrayItem(haystack)
    }
}