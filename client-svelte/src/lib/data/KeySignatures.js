import Prando from 'prando'

import keySignatures from './key-signatures.json'

export default class KeySignatures {
    static randomWeighted() {
        let haystack = []
        
        keySignatures.keySignatures.forEach(key => {
            for(let x = 0; x < key.weight; x++) {
                haystack.push(new KeySignatures(key))
            }
        });
        
        return new Prando(Date.now()).nextArrayItem(haystack)
    }

    static random() {
        return new Prando(Date.now())
            .nextArrayItem(
                keySignatures
                    .keySignatures
                    .map(key => new KeySignatures(key))
            )
    }

    constructor(keySignature) {
        this.name = keySignature.name || "Unknown Signature"
        this.pitches = keySignature.pitches || []
        this.root = keySignature.root || ""
        this.scale = keySignature.scale || ""
        this.weight = keySignature.weight || 0
    }

    vexflow() {
        return `${this.pitches[0]}${this.scale === "minor" ? "m" : ""}`
    }
}