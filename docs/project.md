# The Jam
✅🟠🚩 = status
* = current task
♯♭

## References
- https://en.wikipedia.org/wiki/Circle_of_fifths
- and like all of wikipedia's music related stuff
- https://yourcreativeaura.com/sheet-music-notation/
    - really good info on the names of all the different pieces of sheet music
- https://musescore.com/user/73972/scores/1352796
    - sheet music to reference, with key signature and time signature etc. 
    - undertale was a coincidence

## Todo
- * practice page
    - ✅ picks a key

    - ✅ shows all the playable notes on standard tuned bass guitar
        - ✅ combine rests
        - can bar lines go at the end instead of the start?
        - ✅ modify the range of notes by a list of instruments so that only playable ones are shown
            - and which clef is used 

    - generates an excercise
        - should always (?) start with the scale, 1 through 8 up then down
        - pattern style
            - geenerate a 3 or 4 note sequence based on the scale, eg I, III, V or numbers from the minor sequence or whatever
            - go up and down the scale by single step so 1, 3, 5 then 2, 4, 6 up to the octave, then back down by single steps
        - random style
            - just notes from the scale all over

    - display as full score or as like 1-4 bar "flash cards" timed to practice sight reading
        - basic flash card mode: a checkbox to toggle it, and pick a new random key every second. it'll just keep ticking on
        - perhaps build a svg structure and do it like a full guitar neck with circles over the notes similar to how it's represented in the Bass Theory book
        -     


- ✅ generate the daily jam
    - revisit this to use some of the structure around note data types and stuff for the /key practice page
    - is adding rhyming scheme guidance of value? https://en.wikipedia.org/wiki/Rhyme_scheme
    - is adding a solo section of value? 
    - generate like 1000 into csv and look at them in a spreadsheet and see if there's anything interesting
        that crops up. Especially like, a lot of repitition. It would be good to publish that balance as well to see
        if there is any opinion from people on how to change it. Make like a data page or something I guess, 
        here's a specific set of 1000 seeds to compare apples to apples, and here's a random 1000 to see what sorts 
        of interesting shapes appear or something? 
- visual representation
    - 🟠 "Score" sheet music component stuff to do
        - > A thin double vertical line means it’s the end of a section; such as the end of a verse or chorus.
            - add that in all the right places
        - bottom row should be padded with empty bars
        - textnotes should be the chord ("Cmaj", "G#min" "Gsus7" etc. But I have no idea what chords should be played)
            - ✅ display textnote for root note
            - display 135 chord in treble cleff once I figure out what they should be
        - shouldn't always use whole notes, eg 3/4 time it's inappropriate. need a dotted half tied to a quarter
        - show root note in bass clef
            - this logic is commented out currently
            - bass notes are wrong I'm pretty sure? unclear how to align them on the clef properly
            - connector lines on all bars - some code is commented out
        - !! notes shouldn't all be in the 4th octave ("const keys = [`${note}/4`]" in Section.svelte)
            - not sure how they should go, but not like they are
            - playtesting suggests this is more important than previously thought
        - revisit layout to make it more flexible
            - it would be nice to see more of the score. 
                - I don't know if I can scale the svgs
            - currently doing fixed width grid layout that locks in 5 bars per line
    - tabs
        - https://github.com/greird/chordictionaryjs/blob/master/docs/DOCUMENTATION.md
    - piano
        - like, a visual representation of the keys on a keyboard that are pressed together to make a chord
    - other instrument-specific prompts, if such things exist?
- the site should like, go through the whole thing just chord by chord by chord, in time. 
- metronome on/off, play chord on each bar on/off
- free entry of notes that match constraints, so that people can like, write a part even they don't have anything else or something
- tests

### done
- release updates
    - ✅ get it working on robsdreamco.com!
        - ✅ reference whatever the fuck it is I did with imtg
    - ✅ render everything server side so client just gets static content
- generate the daily jam
    - ✅ notes
    - ✅ figure out chord stuff
    - ✅ time signature (3/4, 4/4)
    - ✅ bpm/tempo
    - 🟠 progression
        - ✅ generate progressions
        - figure out how to parse `♭VII7` and `VII7` and such progressions and move some of those out of inactive
        - validate that the way I do `♭VII` and other ♭-prefixed .. things..
    - ✅ structure (verse chorus verse chorus bridge verse chorse, etc)
    - ✅ bars and lengths and stuff like that - each segment can be 8, 16 or 32 bars
    - ✅ weight the different options
        - ✅ thinking that "weight:10" can mean that there should be 10 entries in the 
            actual collection that's drawn from
        - ✅ run all the options through weightAndPick
        - ✅ start sussing out some weights to try. ranking of frequency of use or something.
    - ✅ sections should generate their properites influenced by the root song properties.
        - the sections are written to initialize themselves from the cre song properties, and
            should be revisited every now and then to be more influenced by other sections of the song, etc
- visual representation
    - ✅ make a jam jar favicon.ico
        - Usually, a favicon is 16 x 16 pixels in size and stored in the GIF, PNG, or ICO file format.
        - https://developer.mozilla.org/en-US/docs/Glossary/Favicon
    - ✅ lightly styled layout
    -   "Score" sheet music component stuff to do
        - ✅ score exists 
        - ✅ https://www.vexflow.com  
            - this seems like the best one to start with. mature, good license, etc.
        - ✅ bars are flexible across different resolutions etc
        - ✅ first bar should have the musical key noted


## implementations
- web site is the start and it doesn't do any video capture it is a prompt
- others are not ordered in any meaningful way
- mobile app - shares the video out to whatever apps
- filters - tiktok filter, insta if it is as common seeming to do those, etc. in-platform implementations
