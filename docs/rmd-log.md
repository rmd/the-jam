# RMD Log

20230406 2121 the jam. the idea is that it is a thing that generates a combination of key, chord progression, rhythm, time signature and like, other stuff. and then you record and play along with it. 

so its mostly a tool for me to start with and to learn things about music with. 

I've been thinking about doing it in a game engine. I've looked at pixi.js and Phaser - that's what's in the first-attempt folder. and it spilled out, probably just pixijs stuff or something? 

anyway, I got deterred from godot because godot 4 doesn't export to web in a way that mac os or ios can interact with and like, that is irritating, yknow?

20231001 1121 there's been more than one time where I wanted this to exist in the past six months and I'm at kind of a lull between projects and games and so I'm going to give this a shot. 

I should try soldering some evening this week. That's a tangent but it helps me to write things down.

1125 okay so the things - well, okay. I have no idea what I did here before so I guess the first step is seeing what there is. 

oh I'm doing something with pixi or whatever. Okay, uhhh, I'm going to get rid of that stuff. 

1128 .. I might use TypeScript. No, not this time. 

1129 interesting defaults for linting, formatting, and two types of testing 

1208 so, this is the stalling out point for me:

https://en.wikipedia.org/wiki/Chord_notation

https://en.wikipedia.org/wiki/List_of_chords

Do I want to support _all the chords_? If I do what sort of weirdness sneaks in? If I don't, what sort of weirdness do I exclude? 

So I think what eventually I want to do is weight them, maybe? But for now I guess like... put all the chord types in? 

1523 maybe this one: 
https://en.wikibooks.org/wiki/Music_Theory/Complete_List_of_Chord_Patterns

1558 yeah I'm feeling okay about that one. 

1900 so when I left off I had it saying the note and the chord. So next is progression and rhythm and bpm. And then perhaps song structure, chorus verse bridge verse chorus or whatever. even bars and lengths and stuff in time. 

1926 instead of suggesting a specific.. beat I guess, like samba or waltz or whatever, I'll just do time signatures and if people want to blend whatever doodad however, maybe it will work I don't know. 

20231003 2052 bit of time this evening. what's going on? 

2053 progression is next.

20231004 1956 I just used the word "components" as part of a commit message as a super class I guess of all the data classes I've been adding in and I guess if I ever need to add logic there is a superclass but I don't think I do and I can wrap it all probably if I do but anyway, when I started writing this it was only going to be about what a poor choice "components" is for that category or superclass or whatever in a project that has an interface.. component.

1958 So there's utility in having a bucket phrase for it but component is the wrong one. Maybe there's actually one. 

1959 also all of this is for like, pop song level complexity stuff so definitely some of the rarer elements are going to have to be either removed or down-weighted heavily. The latter for now, why not have some wild edge cases?

So what's next?

Structure, bars and length and stuff like that, and then weighting the options. So the easy stuff is probably done. 

2001 Chorus and verse are not in wikipedia's "Musical Terminology" category: https://en.wikipedia.org/wiki/Category:Musical_terminology

perhaps that is because there is a specific term for what those pieces are? 

2003 Is there often like a key change for the bridge? 

oh this is a reference I want, the tab is already open. I knew I'd seen something: https://en.wikipedia.org/wiki/Song_structure

2006 So structure wise, we don't always want a bridge, but if there is a bridge it might be in a different key, time signature, chord progression or any combination of those three. . 

2010 hmm. I made a `makeBridge` method but perhaps there should be something more about the structure of all of it. 

2012 this could be a mobile app that records the jam and then just lets you share it on all the different platforms or to your device or whatever. Anyway, keep working with this because it is quick.

2016 chord progression should change almost all the time for a bridge, so just pick a new one and then whatever percentage of the time the current progression is weighted for it'll happen again maybe. see how that feels.

2017 so what I'm trying to figure out is how to do the structure. So I guess I need to get a list of structural elements and figure out how to bias things to a reasonable extent

even if the majority of songs are ABAB or ABABCB more day-to-day variety would kind of be entertaining

2021 oh also some of the earlier choices will influence structure, like 12 bar blues is not just a progression but also a structure .. or maybe not like chorus verse structure but length of those things structure. hmm. 

anyway, be aware that some earlier elements of the jam frumpy dumpy do something. 

exist and influence later ones. that's it. 

2023 and these pieces aren't like the other data pieces, or at least selecting them isn't. So I'm going to list them here and then just hard code stuff until I feel it better.

2024 evening snack soon, too.

so structure-elements:
- intro 
- verse
- pre-chorus
- chorus
- post-chorus
- bridge
- outro

2028 So like.. AABA or ABA are common and ABABCABCAB exists so a little stuck on where to go with this. I'm sort of thinking like length of structure is 3 - 10 and the bias is heavily towards AABA and ABABCB so 4 or 5 should be the most common lengths, and for each of those lengths those are somehow the more dominant pattern. 

Or I could just do a list of core patterns and then see if the non-bridge optional elements [intro, pre-chorus, post-chorus, outro] get applied to those bits. 

- AAA
- AABA
- ABA
- ABAB
- ABABCB
- ABABCAB
- ABABCBAB
- ABABCABCAB

I think that's going to be a good solution to start with at least. After snack and perhaps not today.

20231005 1958 So plodding along 

20231007 0756 
0819 so pre and post chorus decision making. I guess find each of the Cs and determine if there is one

0901 pretty content with this. 

Had a thought that in terms of determining length, each section can be 8, 16 or 32 bars. That's a simple start.


20231008 1142 so, the - hey 1142AAAAA! - the way that chords are rendered online is an actual standard, which shouldn't surprise me but it sort of does.

https://chordmark.netlify.app/docs/getting-started

However, I don't think that's what I want here

1144 https://github.com/greird/chordictionaryjs/blob/master/docs/DOCUMENTATION.md looks nice for tabs

1220 vexflow is very interesting and I think probably a pretty good starting place but I'm wondering if I should just find a tool that renders _and_ plays. Is there such a thing? It seems like something that plays a vexflow should be a thing and if not it might be an interesting quagmire down which to bog myself. 

I edited the end of that sentence several times and quite like it. 

1222 where it landed, even.

Okay, so vexflow. Go ahead with that or find a player first? I'm going to keep looking for a player for a little bit.

1230 VexFlow sounds like it is relatively bare bones/close to the metal (to the extent that a js library can be) and it seems worth learning at least a little bit. So I'll start here and then decide whether or not it is the right tool or if I should try something else. 

Some of the challenges others seem to encounter with it is that it isn't great for real-time editing but I just need stating output to start with. Scrolling along through it all/playing the audio can come later.

1303 okay, I have vexflow integrated in a basic way, rendering a hard-coded score from the example not anything generated from this code. 

So there is going to be a few things to do related to this:
- render root note in each bar 

oh I guess I don't need to do a full chord or anything like that probably I don't want to limit things to like, major chords. Just get the root in as a whole note for each bar for each bit of the song.

will need something that calculates the progression and stuff. 

So what's happening? I guess the progression should be the base multiplier for the bars shouldn't it? That's what a progression suggests right, the sequence of notes to work through in each cycle. 

1307 eventually different pieces are going to restrict what options are available for the next part of the song.

Like, just generated:

```
engine go vroom 9
C minor ninth
3/8
Marcia moderato (75 bpm)
Circle progression vi-ii-V-I
ABAO
```

So that's a 4-step progression but in a 3-beats per bar thing.

```
| vi ii V | I vi ii | V I vi | ii V I |
```

oh but that's not right, that's - it's always going to be the number of steps in the progression

| vi      | ii      | V      | I      |

so for this progression, the number of bars has to be a multiple of 4

1310 and for eg `Sixteen-bar blues I-I-I-I-I-I-I-I-IV-IV-I-I-V-IV-I-I` the number of bars has to be a multiple of 16

1429 so given a progression and a key and a index progression I can figure out what note to play sort of

key = A 
progression = I-III-V

I = 0 semitones
III = 

major scale - tone-tone-semitone-tone-tone-tone-semitone

I = +0
II = +2
III = +4
IV = +5
V = +7
VI = +9
VII = +11
VIII = +12

1435 minor scale - tone-semitone-tone-tone-semitone-tone-tone
T S T T S T T

i = +0
ii = +2
iii = +3
iv = +5
v = +7
vi = +8
vii = +10
viii = +12

1501 reading https://en.wikipedia.org/wiki/Key_signature

1542 not all bars get setup with the right number of things which makes an infinite loop when I am trying to make all the bars. 

I guess I could always just do an inner loop or something like that and figure out how to return the right letter but this is also going okay

1553 so I have a test seed that has a fractional loopsize

1554 I might rename the class from `Engine` to `Song` because that's what seems to be happening here. 

1556 so how does the fractional loopsize happen? 

it is happening in the chorus 
sizes 18 4

test seed is 1696794111854

1558 the chorus has a different progression? 

```
IABABCAXBY 

aES1 A 1 9 Engine.js:62:21
prog 
Array(9) [ "I", "IV", "I", "V", "I", "IV", "I", "V", "I" ]
Engine.js:70:21
sizes 9 9 Engine.js:77:21
Loops 1 Engine.js:80:21
aES1 B 2 18 Engine.js:62:21
prog 
Array(4) [ "I", "vi", "ii", "V" ]
Engine.js:70:21
sizes 18 4 Engine.js:77:21
Loops 4.5
```


1606 in theory if the key signature is correct then the thing I need to make sure of when outputting the strings of notes is that the letter is correct and that could be weird. Stepping up through the sharp or flat array might be interesting, I don't know how to blah blah. 

anyway. pick like if it's a G#/Ab, which do you show? it's probably even more important when the key signature is there. Anyway, maybe I'll figure it out eventually. I think that this was described somewhat in the circle of fifths thing, which I should add to project. 

1618 okay so 4.5 loops, where does that come from? It's because (maybe) the chorus has its own progression? 

and bar size doesn't account for that. How did the chorus get its own progression, is it supposed to?

verseSize and chorusSize are different

1623 A and B have different progressions

```
prog 
Array(9) [ "I", "IV", "I", "V", "I", "IV", "I", "V", "I" ]
Engine.js:70:21
sizes 9 9 Engine.js:77:21
Loops 1 Engine.js:80:21
aES1 B 2 18 Engine.js:62:21
prog 
Array(4) [ "I", "vi", "ii", "V" ]
Engine.js:70:21
sizes 18 4 Engine.js:77:21
Loops 4.5 Engine.js:80:21
aES1 A 3 9
```

1624 okay, got that sorted, there was another spot where I was testing for "B" thinking that meant bridge. Section characters are probably worth revisiting for clarity's sake but whatevs. 

BUT now the bridge is getting a fractional loop because it is getting the wrong size at some point. In some way. 

```
aES1 C 5 27 Engine.js:62:21
prog 
Array(4) [ "I", "vi", "ii", "V" ]
Engine.js:70:21
sizes 27 4 Engine.js:77:21
Loops 6.75
```

sizes 27 is bad. setting up the bars doesn't account for bridge correctly? 

1628 hey, there we go! 

```
aES1 C 5 12 Engine.js:62:21
prog 
Array(4) [ "I", "vi", "ii", "V" ]
Engine.js:70:21
sizes 12 4 Engine.js:77:21
Loops 3 Engine.js:80:21
aES1 A 6 9
```

1629 so now I should be able to restore the loop

1644 oh interesting: 

> The Stave APIs were designed such that you don't have more than one bar / measure per stave. The idea is that you render a new Stave for every bar, and either attach it by juxtaposing it to an existing stave or rendering it in a new row

So in theory I could render each bar in its own div and use css grid stuff to flow the rendering of them. 

For now focus on rendering each bar, then take steps from there. 

So from this point I don't think I can do something that is specifically "here's a bunch of letters" given to the thing. I suppose I could maybe do some junk but I don't know. 

eventually I should blend together all these arrays in the song object into a.. something. I don't know where I was going with that.

ANYWAY. render each bar on its own. 

1647 easyscore is interface, it shouldn't be in the engine anyway. This engine/song class returns the array of sections - section object is probably where I was going. section can have bars each bar blah blah. anyway, so conversion into easy score or however I go about doing it should happen svelte side.

And I'm not sure I want to do the easy score thing, going down to the base api feels better to me at this point. Maybe as I learn more i'll appreciate easyscore more. 

1649 yeah the base api lets me make chords like this:

```
new StaveNote({ keys: ["c/4", "e/4", "g/4"], duration: "8" })
```

1656 I'm concerned about hard coding sizes but if they're SVGs there will be ways for me to like, scale it around and shit probably? Or maybe different units are possible or something. Options exist. 

1658 okay, dinner prep time. The next step is to take the CompleteStructure content and loop through it and convert the roman numerals to actual notes to send into Vexflow.  Work in Score.svelte

1084 loop through it. Notes. 

1819 I'm not sure I need chord patterns except that I do need major or minor right? Except the progression tells me that maybe? `III` vs `iii` so maybe patterns like that are something that the humans can decide on? I don't really know at this point so that will be.. something?

1821 hmm! that will be interesting to understand, perhaps. 

But for now I need to translate from "A" and "III" to

1831 so if I give it "E" and "III" e is index = 7. "III" is up 4 semitones, which would make noteIdx = 11. That all checks out. 

if I give it "E" and "vi", e is index = 7. "vi" is up 8 semitones, which would make noteIdx = 15. 15 is greater than 12

15 - 12 = 3

sharpkey idx 3 is C

so is C vi of E?

if I repeat the array elements and count manually up to 15, yes it is. So 3 should work. 

But I'm getting 12 for some of the things.

1903 eventually will probably have a sub component in Score that is a Bar svelte component, and Score can loop through its notes and shit and pass them all down to instances of Bar something something something like that. 

1906 okay! I'm drawing bars! So yes, given that I'm going to want each bar in its own div I think I should make a Bar.svelte component and figure some things out from there. That way each one can have its own height and shit and I can flow them around each other probably.

2135 time to stretch. I have a Bar class but it isn't rendering when it is just on its own. 

```
Uncaught (in promise) Error: [RuntimeError] NoYValues: Can't draw note without Y values.
```

is the error I get.

20231009 0804 so there's an error that I get. I don't want to get that error. 

0824 I think I was doing STAVE_HEIGHT for the 7 so it would be drawn below the viewport, basically. I think that's what the old one was doing wrong, and so it was like an out of bounds error or something.

0829 it might be worth pulling the clef and time signature stuff out of bar and have like a FirstStave class that does it or something. 

0917 I feel like I need to comment "yummy breakfast" or something to indicate I haven't been focused here during that period, but like.. why? Well, we know. Anyway.

So keep on keeping on. The bar class is stabilizing well.

0919 so try rendering the rest of the bars.. 

0920 works nicely! I'm concerned about potential performance issues of multiple renderers but it certainly isn't obvious from the example of the seed I'm working with which has 10 sections so is relatively meaty.

There is a lot of padding between all of them but that's okay. Soon it will be time to blah blah blah. 

Okay, so I think it's worth rendering a stave with the time signature and clef at the start, and letting the rest of the bars do their own thing. 

1013 add key signature to chart next? 

Basically I guess I'm working on taking this interface and algorithm blah blah and making it a nice presentation. It _can_ be shown off in its current state and this is the bells and whistles pass for me to feel more comfortable with it. 1.0 is very near, although weighting should probably be in 1.0. Anyway, doesn't matter it's not _live_ live or anything.

1015 so there's a set of major and minor signatures that the thing recognizes:

```
https://github.com/0xfe/vexflow/blob/master/tests/vexflow_test_helpers.ts#L399
```

and 

```
    keySig = new KeySignature(keys[i]);
    keySig.addToStave(stave1);
```

is probably how it gets added to the thing. 

1018 sort of but it was just like adding the clef actually, an operation on the stave. Coming in with the bridge will be interesting, need another first bar. 

I may end up merging first bar back in to, I'm not sure. Having a smaller thing with fewer branches has very very small performance benefits.

Anyway, when I return, load the page and look at what to do next. The key signature is an easy place to start from.

1332 think I should refactor song such that it is a collection of sections. each section has a type (bridge, chorus, etc) then when rendering it each section can render its own bars including the last bar of the session so it has the double line, etc. 

1357 I'm doing some fucky shit to get key signatures working properly apparently. This is probably gross, and lends wait to that notion about sections. Anyway.

1409 yeah kind of flailing a little bit, just bashing my way through resolving the changes I made a little while ago. Well, I guess some of it is removing complicated chord progressions and dealing with all the variants. 

Some things work but the options crash after a couple of refreshes, like 4 tops. Because of some problematic combination of things.

Right now I'm told G# is an invalid key signature, and yeah it probably is. So I'm not sure how to deal with that. 

Maybe instead of sharp keys and flat keys I need major keys and minor keys? But this isn't really a _key_ so much as the note that I want to play, so bleeeeech. fucked if I know. 

1515 Alright, I've just done 50 loads with timestamp-based seed (so, deterministic technically, but effectively random and new every refresh pretty much)

1837 I think I want to get the bass clef in. Are things working okay? 

I probably need something that maps 88 keys to their veflow equiv or something, I'm not sure why I just thought that but I did.

1840 I think it might be better if I refactor the Song class now that I have thoughts about sections and things.

Okay, so what are those thoughts. 

A song is made of sections. 

There are several types of sections, including verse and chorus.

Each section can have its own.. characteristics? Key, progression, time, etc.

And then on the rendering side a section can render itself etc. 

1844 so .. start with a completely new song class? 

1845 I was just about to say "Sure, it's a jam not a song" but I like song. So I'm going to stick with song and just do new things inside of the current class. transition, not complete refresh.

1900 garbage nighth clean up time. Back later. 

2106 right, refactoring stuff. 

2231 in theory I have the data right, or close to it, and am working on updating the score so that it renders from the sections.

I will also have to update the header to show the details of the first verse or something like that, that's the like, key of everything in the song. 

Or should a verse adopt its songs details outright, and a chorus _probably_ .. well it probably changes really, and a bridge almost always should be different. I think? W

20231010 1754 
1759 just about dinner, so not a lot of focus at this time. 

1801 is the data right? I don't - oh, yes, I think I know what I was saying? Maybe? The data is mostly right though probably chord progressions should change in choruses too? did I do that? 

probably there's typically a relationship between whatever the progression used in A is and the ones used in B and C and X and Y and I and O. Like.. anyway.

Random selection is a prefectly cromulent starting point. And also kind of the fun, the point is to be broad isn't it? Different difficulty levels or something. 

1832 tracing a problem. KEYS IN BAR, a console log, happens once but I've been poking around the NOTE IN BAR outputs. 

1833 so "V" is getting passed in as the note and yeah, clearly that's wrong. That's the chord progression reference it hasn't been normalized. Where should that happen, in Section probably? That's what would convert things to blah blah blah. the key and the point in the chord progression

1848 so it is in song.makeSection that the problems begin. Because of bars. Bars is an array of all the progressions.

1849 oh I think I see. I was thinking about not passing section a section object because it binds the interface and the data closely together but I mean, fuck it. This is small enough that that doesn't matter too much, and when it does there will be resolutions to explore and therefore fun to be had. right? right.

shhh.

okay. 

1852 sections need key and time signatures. they have a type which is good, that identifies them helpfully and can go from there.

But a bar can't just be a thing. It isn't. 

1853 that's fuzzy alright. So Song needs to be updated to add the necessary stuff to the section. And bars need to have the progression _and_ the note? Yeah it should be okay to convert from key and roman numeral to letter in the song. 

1858 section should be its own class and instead of makeSection() I can just new Section(). And then it can make changes and stuff. 

2153 I have a lot of changes made and I think the output, like the Song with its Sections, is good, and the visuals are coming back in steady steps now. It's going to take me some amount of time to identify everything that is unused now.

But I think we can get there.

2154 There is a lot of complexity yet to come in sorting out and representing how the different sections interact with each other and with the core properties of the song. The verses I think should take the song's properties, but anyway. Also each section is completely doing its own thing, it .. shouldn't. I guess this is all the same thought mostly.

2201 stalling out a bit on next step. So, right now there's a big disconnect between the stuff that the song picks and generates for itself and the stuff that the sections use. Including the steps by which all the sections are created. 

So part of making the sections should be setting the song structure value from the generated sections. Not generating it. 

2216 that was a very non-functional-programming commit. side effects upon side effects. well, not tons but like, making structure then rewriting it after making the sections is .. probably not necessary and a little ugly.

2218 but! stuff! is happening. Sections are their own thing now

2220 I'm at a "what to do next" point I think. So yay me! I might be able to build and deploy this version bound to dates.

2233 left off trying to get a build. tried the static adapter, probably need to use the node server adapter and then figure out how to make things static or srever rendered, etc.

Yeah I guess there's a lot of dyanmism in these pages. wachow!

20231011 2208 the build is a challenge. it doesn't work, which poop. In safari vexflow doesn't work, getting error:

```
SVG context requires an HTMLDivElement
```

so I tried adding another div around Bar and FirstBar but that didn't work. Might need to .. dunnon what. Svelte might be fucking somethign up? hard to say.

2211 discourage!

no just in built version, in dev version too. But the stuff on the site works, so the vexflow stuff should work in safari I have just done something odd. 

2212 oh I don't think I define the score variable anywhere. 

2215 oh I created and bound a variable named `container` but in the renderer I was giving it `score`, which wasn't right. 

2216 and safari doesn't wrap automatically, so I will need to css grid it

20231014 1055 it isn't wrapping in firefox anymore either, so something about how I've done the bars breaks it. I added a div in somewhere I think, that was probably it. 

1112 I popped the things into a grid and it's good, but each cell is padded. I guess I could try with the same width I give to the SVG stuff. 

1113 yep, fixing the width locks it in okay. But then it is all fixed width. Which it was going to be anyway I guess?

I might actually want a flexbox layout for this, or eventually to learn more about the grids so that there's just no padding and that the auto fill stuff works, etc. 

I guess I could make the max width equal to 5 bars but let everything else flow? 

Anyway this is fine for now since I'm desktop but I'll make a project entry.

1123 the Score renders quite nicely now. It is fixed width as noted about which is a downside but pretty cool overall. 

I guess add in the bass stave now? 

1126 that's what I'm working on, getting a bass stave in and displaying. 

1422 progress is made. Bass staff is in, connection happens sometimes. having it flow will be weird but maybe there are css targets or something for something or other and I can hack in styles on the svgs to show or hide different aspects. or something.

anyway, what the fuck. oh right, bass notes. I need to add the key and the time signature to the bass clef and what I"m worried about is that the notes are rendering without consideration of the fact that they're on the bass clef and so that means I'm going to have to transpose or otherwise figure out the right note? If that's the case, I might just leave a single treble clef. 

So, confirm that, then decide. 

I'm not sure vexflow is the best tool for this. It is a good tool, but it is not an amazing tool. But it's pretty darn good I don't want to be dismissive, it just doesn't line up with my use cases as well as I would like, from what I'm able to see of it. I am definitely .. limited in my understand of both its capabilities and its functionality.

1426 ugh, right, and I had to add 30 extra pixels to the first bar to have the connector in so the pretty layout goes away. 

Yeah, okay, so I'm going to comment out the bass clef stuff for the time being. I want to keep it around but I don't want to wrestle with it today.

1504 to be able to put a chord on I need to to much better at calculating what the next note is. Probably the vexflow converter thing should output the vexflow syntax "note/octave" pattern thing? And then if the counter loops then the octave goes up or something.

Sigh. 

I don't want to do that I think. I want to get it building and deployable. What's the fucking point of all this if I can't deploy it easily?

And kind of, what's the point of the svelte right now? Some nice basic style stuff but I could .. well, I want some kind of component tooling, and this gives me that. 

ARguably the right thing to do is figure out how to make all of this do the work server side and just send html, css and svgs across. No need for javascript for any of this right now. 

Seems worth reading about. 

1514 http://robsdreamco.com/jam-with-us/ is responding! I got the static adapter working. 

I should add a /random path and get the seed coming from whichever path it is that's loading. Root for the daily, random for whatever. and maybe a seed/{seed}? 

or just /seed/{seed}, and if {seed} is empty it uses the timestamp. That's worth trying. 

1900 cleaned stuff up in anticipation of people poking around. build breaks though, so that's what to look into next.

2036 about to attempt to figure out the build problem again. I think I am content enough with the way it looks right now.

2052 more word editing actually. but now, build investigation!

2056 ohhh it is complaining that it can't pre-render the page in `/seed/[seed]` because literally any value could go in there I guess. That does complicate matters a little but but IMTG can do it so this one can somehow as well. 

Oh except .. it doesn't. IMTG doesn't use slugs like this for param values. 

2103 there's nothing in here that can't run fully in the browser, so wtf. 

2111 maybe I can switch the route to a querystring param. 

2130 fuck, bedtime. Anyway, the switch to qs param I think is the right one, but the build right now isn't putting the right paths in so I'm getting 404s. I updated most of the paths but there's still a couple I guess?

2153 so the problem is that the build expects it to be in the root directory, not in jam-with-us.

there must be a way to configure the builder to be able to handle this. 

2159 base property in svelte.config.js in theory...

I don't see a base in index.html

2212 Bother. Just fucking bother. 

2216 maybe the base property is in the wrong part of the config file.

2219 that fucking did it! High five!

okay so sneak it out to efts

2226 and uh, posted it to mastadon too. why not!

20231015 1126 Okay, so hit a release point. Nice. I have yet to actually sit and work with this thing myself, record myself doing something with it etc but now that it is public sort of I can. 

I wonder if I should label notes. Probably? Does vexflow make that easy? 

1128 I've started poking into the vexflow code. I'm starting to understand it more. Not even like anything resembling meaningful comprehension of how it does what it does but being able to determine and understand behaviours directly. Which is good. 

1131 getYForTopText isn't what I use to set text, but it does strongly suggest that text and notes are possiblel, and perhaps even bottom.

1132 `getYForBottomText` well that sure makes the case for bottom text. 

1146 here it is: https://github.com/0xfe/vexflow/blob/master/tests/annotation_tests.ts#L299

the exemplar test.

```
    staveNote({ keys: ['f/4'], duration: 'w' }).addModifier(annotation('F'), 0),
```

with `annotation` being a small funtion to setup and style the text and stuff. alrighty that seems straightforward.

1159 I have annotations showing, they're just super ugly. Hmm. I'll have to futz with the various options and see if there's something I like enough.

The other option is to use the stave annotation in Bar to put the note at the top where the section mark is. Or better yet, see if there's a similar sort of annotation I can put at the bottom of the bar. It seems likely, the trick with vexflow is finding where it is exposed. For all I said above about poking through the source I am still mostly just wandering about getting a feel for things and forgetting where I saw and what I saw. 

1842 short stint here, cleanup time at 7 but I do want to try getting that text going. 

1844 "TextNote (SVG): TextNote Superscript and Subscript"

This is the challenge. A TextNote presumably is something other than an Annotation. And it's a good distinction, they clearly mean different things, annotations are much more meaningfully connected to a note, it's great there's a ton of options. It just takes some time and that's the fun of it anyway.

1846 and I need to interact with Voices.

```
    f.TextNote({ text: Flow.unicode.flat + 'I', superscript: '+5', duration: '8' }),
```

this is super powerful for lyrics and such. anyway, I should be able to do stuff but 

oh this example is with easyscore. I don't think I want to switch to that right now. 

1859 okay I have it showing up as a text note but the alignment is off. My best guess right now is that I've set things up in such a way so that the renderer or formatter or whatever thinks the text note is in the second half note spot for the bar? I don't know, that's a stretch it feels like. 

Interesting behaviour is that when I set to justify CENTER, it is a teensy bit further left than when I justify LEFT. That's what makes me think it's under the non-existant second StaveNote or something. 

Anyway, progress. And now dishes. Yay.

2002 one good thing about getting the bass clef working, once I am able to do that, will be all the space in the middle so that annotations only rarely get in the way.

2012 Okay I'm pretty good with how the labelling ended up, finally. The solution was basically to make another stave for the text notes and then they get positioned in the same spots. 

20231017 2205 there's now several labels around the score, bpm and sections and letters. 

2210 the thing to really get into next is the weighting. I had some ideas about that, values that need to add up to 100 and then... I'm not sure what I do with that. If I stick to ints I can just dump that many entries into a 100-element array. And if I end up off a bit, oh well. 

2227 bedtime just about. Got basic weighting in place for keysignatures, and made a weighAndPick method, just need to call that for the other stuff in song constructor. If I send the right information to it, I'm pretty sure none of the other things need to change.

2123 I've put in weights for key signatures. I'm looking at how I'm assembling sections and stuff and its still super rough. 

20231019 2115 I am doing bad things for making sections. I have a bunch of "makeIntro" and "makeChorus" or whatever that all call makeSection, and in there I start checking for types. Just.... not good? But ugly, then clean it up.

2116 No I should just get rid of makeSection and have them all do their own thing.

2222 a nice timestamp. I'm working on making sections and stuff. Something is setting the song's progression to the tempo/bpm return and I'm not sure what, so that's where to pick up next. It is the error in the console when the page is loaded. 

20231020 2005 oh right, so things are busted. 

2006 restarted and updated firefox. will it be faster? tabbing in would have a several second delay. Now it is responsive, though not all my tabs are open. There's < 10 most of the time though, so. 

2009 right, so a progression is getting populated with tempo/bpm. in a chorus. 

2010 the verse that appears to have been made previously had it work fine. Logs might not be great because values change sometimes? 

2011 so the _song_ is generated with progression wrong. Verse fixes it by picking a new one. 

2016 this is really weird, I'm doing something really weird somewhere, because I'm getting 

```
Uncaught (in promise) TypeError: console.log(...) is undefined
    Song Song.js:26
```

but that line is:

```
        [this.tempo, this.bpm] = this.pickTempoAndBpm()
```

2019 and the error only shows if the console log is after `this.progression = this.pickProgression()` but there's nothing weird in there I am pretty sure!

2030 adding a semi colon. This works:

```
        this.progression = this.pickProgression();
        [this.tempo, this.bpm] = this.pickTempoAndBpm()        
```

this didn't:

```
        this.progression = this.pickProgression()
        [this.tempo, this.bpm] = this.pickTempoAndBpm()
```

so, that's kind of wild. There's probably a better way to do all that, pass an object back or something. but this seems to have cleared things up for now?

2041 so I can work on weighting values again I guess? 

2042 it does seem as though the keys are more typical. 

oh, in [object Object] time

2042 so I may as well weight time-signatures.

2050 I couldn't find a list of how frequently they're used or anything like that so I just put in some numbers that feel some kind of way.

2132 I added weight numbers to chord progressions. I have no idea, so I'm starting here.

20231021 2052 so what else do I need to weigh? I am through most of them, I think. 

2121 alright, added gut-based weightings for other things. I suppose a good thing to do would be to generate like a thousand of them and see if .. something? How much duplication/repetition there is? Ideally over a week say there'd be minimal duplication. A couple of ABABCB songs or whatever, several probably songs in 4/4, but how much feels like too much of the same? etc. 

20231101 2103 too many bpm changes. it's not practical to record oneself with a lot of bpm changes. 

2110 I tried playing the song .. yesterday I think? In the currently live version which is quite old. I haven't done much in a while but even what's been done is a big improvement and tonights significant dialing down of tempo changes and other properties too, just reduce needless irritating things. 

I'd like to get the bpm only showing if it has changed, and then switch it back to the top probably. 

OH! and I started this entry to emphasize that I need to not make all the notes in the 4th octave. If we loop through the note loop the octave needs to increment as well. 

2201 I'm trying to do a previous value comparison to determine whether or not bpm has changed and therefore should be shown again and it is surprisingly confounding. 

Mostly maybe because I am trying to manipulate an array instead ofmaking a new one maybe?

2217 okay! nice! in a more stable spot. I'm happy with this as a stable point I think. Will try to play along tomorrow and see how it feels. Autoscroll feels more important every day but ANYWAY.

2305 so late! The next thing to work on is getting the notes ascending when appropriate in the progression, so like if some roman numeral - pretend it is V I'm not doing the math - gets us from C to A, the A will be rendered lower in the staff because the notes will always be C/4 A/4 but since we are going up V (still not doing the math) from C, the notes should be C/4 A/5.

2309 that stuff is noteFromKeySignatureAndNumeral  in NoteConverters.js

so when we set up the notes array it will need to be in `c/4` and `a/5` type syntax here. 

0843 so I officially recorded my first full daily jam yesterday. Didn't post it anywhere because that is perhaps a step too far for right now in part because it was pretty bad but anyway. As a user the worst thing was how it is impossible to see the whole thing the way it is built. 

My inclination is to have an autoplay and the way I was thinking it could happen is to change the background colour of the active bar and then reposition the viewport so that the active bar is in the middle (or as close to the middle). A timer that goes a 60000/bpm to get it in milliseconds right? And then it changes whichever cell is active and repositions the view port. Something like that shouldn't be wildly difficult. 

- set a timer for 60000/bpm
- when the timer goes off
    - clear the current active bar
    - determine the next active bar
    - set the next active bar to active
    - update the viewport/do a scrolly mcthing
    - set the timer again

eventually the FirstBar should like, flash in time to the beat or something like that too.

20231105 1536 I think autoplay is the most important feature. Getting the notes right would be great and other forms of tab would be great but autoplay is for me the biggest barrier. 

So start with just a metronome sort of thing. A play button that then flashes at the correct tempo. 

1600 Got a ticking metronome, sort of. Not doing anything on the screen with it, just logging the tick to console. 

I'm not sure how to pass the tick into the different sections. The metronome is in the TheJam class, and the Score class has all the bars and things so .. I'm not sure. 

I want the metronome to keep track of the active bar I guess, and give that information to the score? 

Or it can just pass in beat count. Every time beat count changes it can figure out what bar is active, the tab can figure out which chord chart is active, etc etc. 

2100 so within a section it only has its own beat counts so.. uncertain. 

2101 what about if when the beat happens the component asks the song, like a method on the song gets passed down or something or - this is probably the sort of thing that like events and messages are for. But something needs to know where the bar is and I guess that makes sense that is be the things. 

the section's. It needs to know which beat counts correspond to its bars so it needs to now which section it is. so I guess here comes a section index variable? 

2121 I am being confounded by this for the time being. Passing information down, reactivity, these things feel beyond me in this moment. I know they will return another time though. 

2122 I do have a pretty decent amount of infrastructure of this stuff in place. I'm actually pretty close, I think that I'm not necessarily having section react to the change beat count appropriately and so the bar never gets set to active. it's not constantly being redrawn right? 

so in section have a function that gets run every time beatcount is updated. if beatcount mod 4 = 0 then set the background of the bound dom object to yellow. Otherwise set it to white. 

20231111 1911 good notes, Past Rob! I got as far as having the function that gets called but I haven't gotten the active bar changed in any way. 

1923 figuring out how to count the bars is proving a little bit troublesome to me. 

    beatCount goes: 1, 2, 3, 4,     5, 6, 7, 8,      9, 10, 11, 12

1924 it's not broken up like that in any actualy way I'm just breaking the beats out into bars. I'm doing a lot of div 4 stuff but I'll eventually need to use the actual time signature. However.

1930 I'm just not quite getting there. 


beatCount % 4 goes: 1, 2, 3, 0      1, 2, 3, 0 ... 

so I guess if beatCount % 4 == 1, increment barcount? 

1948 getting closer but still not really like.. getting all this. I'm trying to figure out whether or not this bar is active and I'm doing weird shit sectionIndex * bar index doesn't work because in section 1 the count goes up but then section 2 it's going 2, 4, 6, 8, ... 

so it has to be like.. the length of everything to this point in the bar. A section doesn't know what number its first bar is in the universal sense. 

so like.. a bar offset that the section has to calculate for itself? If the intro is 16 bars then the verse needs to know that its offset is 16. 

so in a loop have the current offset and every section it increments by the length of the previous section or whatever? I'm getting close to it in my head. 

2000 I'm not calculating the offset right, in that it isn't being all dynamic and stuff as the ticks occur. In Score.svelte the method there gets called but then never again. So 

20240109 2020 want to build a thing that is: 

- pick a chord (hardcode G to start)
- pick a progression (hardcode "I" to start)
- pick a third thing I forgot that can be ignored in the early implementation

and then it creates a staff that wanders around that chord for 32 bars, or follows the progression for 32 (or some appropriate number) of bars

20240110 2203 making small progress on the above. I don't remember what notes look like in vexflow and I've been trying to get .. them? an example of one so I can play around with the math.

2204
2214 I have a bar kind of existing and some html components are rendering to the page they are not visible. 

the svg is in the elements but it isn't like, black. it's all white.

something in the way this page's html is constructed is flawed. 

2228 if I copy paste in Bar.svelte's bootstrap method it works okay. 

2235 I still don't know what was causing the rendering fail but I was able to convert the code into something I can use. Also, I finally realized that one of the options that can be passed to the note is the clef on which to do it. 

So {note}/4 gets things basically on the treble, and /2 on the bass.

The e string of a standard tuned 4-string bass is note e/2. 

2240 so now I have a single bar staff rendering with a bass cleff and a whole e/2 note. It's a start!

Next step is to get note to be the key. Bed very soon but sneak this in maybe?

2248 doing that now. next problem is that a lot of notes are rendering lower than the bass' range 

I'm kind of tempted to make a table of piano keys or something along those lines, numbered from 1 to 88. Then I can specify a preferred range. Each of those can have a column/be mapped to a vexflow note, so E2 is piano key 20 (if this very short and unverified google search is close to accurate. representative in either case).

then the .. thing that like, modifies other notes or something I'm not sure I'm making sense right now. 

Anyway, with that set of 88 keys I can do a bunch of stuff like setting the acceptable range of notes to be in the bass-friendly range and so be it. 

20240111 2026 I started making a "drawBar" method and I got to notes and that gets tricky because there can be multiple notes. So I guess it has to be an array of arrays of notes or something like that? 

Like if I wanted to do .. ugh this is going to be weird. I can't get too far ahead of it so for now I'll just do one set of notes. That's all I want for bass practice right now anyway, don't need no steenkin' chords.

2128 A flash card thing is relatively quick to build. a checkbox "flashcard mode" or whatever and it just updates every second with a new key and root note displayed that's simple to build and a good starting point. 

2206 this rudimentary flashcard mode is in place now. Want to launch it. publish it, however I want to call it. I guess I run build and scp that up?

2223 I can't get the /key page to be found on the web server. I keep getting a 404. 

2226 I got it to prebuild a key.html page, but that just says 404. 

2233 it dynamically says 404 though, that isn't static in the page. 

2240 finally got the configuration that works for me in +layout.js. prerendered pages and always trailing slashes outputs things in a way that I don't have to fuck with apache to get to work. 

20240112 2022 wrote this earlier today (1407) while practicing bass:
-----
giving my flashcard toy from the-jam a try and it has promise. It shouldn't .. it doesn't need keys although I guess the key learner aspect of it does so I'll stick with it but it doesn't need weighted keys it need to pick entirely at random because I want to pick some representation of that note on the bass to play. 

showing the fret markings for each one would kind of be helpful but that maintains the dependency which I don't want to do. So need to change, but the weighted keys are mostly G, A, C which means the weightings are probably good, I've seen B and F and various sharps and flats of all of them. 

So now what I'm thinking is that there's the lookup table of 88 keys, and the piano's range is defined as [1..88] (offset as needed in code but for now) and the standard tuned bass's is like... [whatever number it's lowest E is ..whatever it's highest fret on the G is.]

so some information about instruments. bass likes bass clef/f-clef, it's range_low is key30 (or whatever) and its range_high is key50 (or whatever). 

then like, it will have string/fret combos that map to each key as well? or each key is the key to a map entry whose value is a list of (string,fret) coordinates. 

1413 I hope I remember to look this up but I think I will. this is a good reference for me to have I think.

tones? from 1 to 88 (maybe the human-hearable tones are different than a pianos? probably but do I need to worry about it? I'm going to say no, updating a bunch of instrument data will be a pain in the ass if it needs to be done but it won't be complicated, just a lot of magic number changes that will mostly be mathable if I wanted to bother. 
----

And there's related notes in this document as well. So I think that table is a good idea probably. 

Some sort of enumerationy thing of tones and then some instruments

2026 yogurt soon, but I created some data files to start with. 

per https://en.wikipedia.org/wiki/Hearing_range, we can hear from 20 to 20,000Hz and it also includes a picture that suggests that's C1 through C10, maybe a little bit past but that's plenty. 

2028 https://pages.mtu.edu/~suits/notefreqs.html includes C0 to C8. C4 is middle C so is like, that all I need to worry about? https://mixbutton.com/mixing-articles/music-note-to-frequency-chart/ says octave 8 is a piano's highest octave so that's enough to start with for sure. 

oh and wikipedia has a good table: https://en.wikipedia.org/wiki/Piano_key_frequencies it even has the ranges of some different instruments marked

20240113 0952 okay, I filled in the note frequency files for a typical keyboard range, A0 through C8 inclusive. Next steps will be to setup the bass instrument file I guess with lowest_note and highest_note or whatever that are indexes of the frequencies. Then I can use that information to create boundaries for the practice page

1223 while working 
1227 at a clementine. While working on the guitar data file I thought about the note frequencies and I am going to edit that but commit first so I can roll back. right now I have: 

```
    {
        "frequency": 27.50000,
        "notes": [{
            "pitch": ["A"],
            "vexflow": "a/0"
        }]
    },
    {
        "frequency": 29.13524,
        "notes": [{
            "pitch": ["A#", "Bb"],
            "vexflow": ["a#/0", "bb/0"]
        }]
    }
```

but I'm going to try:

```
    {
        "frequency": 27.50000,
        "name": "A0",
        "pitch": "A",
        "vexflow": "a/0",
    },
    {
        "frequency": 29.13524,
        "name": "A#0",
        "pitch": "A#",
        "vexflow": "a#/0"
    },
    {
        "frequency": 29.13524,
        "name": "Bb0",
        "pitch": "Bb",
        "vexflow": "bb/0"
    }
```

etc. The key is really the name. OOoooh but this doesn't let me do steps, so it is not as good. 

But I like how I can get all the .. how it is keyed on the name, basically. But 

so the hope with this table is that it simplifies the process of using roman numerals for sequences becomes easier right, well just like the number of the note in the chord. So key of G, I is G, III is G + 2 spots in the sequence. any way this is a terrible description but I want to be able to count tones and semitones based on the scale pattern and for that to work I can't double up a frequency. 

But I _think_ I want the core storage of this information to be like this, keyed off name not frequency, and then I can figure out a way to process the array and filter it out so that only notes that are usable are available to be picked from and usable means based on the key and instrument and eventually I'll have to figure out a way to filter the list so that only A# or Bb are in it, whichever is appropriate. I'm sure that will fall into place nice and easy and everything.

1236 So yeah, keep going on this I suppose. I want a repeatable edit pattern but I'm not sure I'm going to get one. I might be able to do option click cursors so that I'm changing all the white key notes and then all the black key notes or something like that? 

1237 I think it will be manual, so here we go. 

something like the scale should have an array of valid pitches, so like [A,B,C#,E,F,G#] (that is not a real scale, probably?) and then I can filter the big list based on whether or not name is in the valid pitches. that will eliminate all the sharps or flats that shouldn't be in the list, then I can crop any of them were the index is less than something. I'll have to add a field, "pianoKey": [1-88] or something like that to the A440 file? maybe? 


1256 aw fuck I just realized I didn't commit the original style oh well lost forever I guess, unless I want to undo a lot. while it's still in the editor, then commit it, then redo all the edit actions. we'll see. For now it's time to clear the driveway - pants first of course. Then make some pizza dough so it'll be a bit but when I come back I can pick up with the editing of things, and decide then if I want to undo to commit then redo to pick up again. 

1431 sidewalk and driveway done, dough started. We're using the bread machine recipe which has a 25-minute warm-up cycle so I'm going to relax a bit and then go clear the patio and portions of the deck. This is the nicest the weather is going to be for possibly up to two weeks, or at least the warmest, so I don't want things to freeze solid. 

So I'm just going to push forward. I think the old format was less optimal. This plan is working, I'm pretty sure. 

1439 okay, pass completed. I'm going to assume there's some errors but probably forget anyway and be confused some time later. 

1440 I'm going to do a pass to clean up the state of the man jam page. I uploaded a broken thing to get the practice tool up

1450 cleaned it up. So now I want to load the list and filter it based on the key. So the keys need to have the scale of notes in them. 

1451 bread maker just started, but I got the first key signature done I think.

1554 working on reorganizing the key signatures file to start at Ab and end at G. There is no G#. It is a messy list right now, I'm not sure what I've done but there were some duplicates and things weren't ordered well. So now things are messier, but on their way to being ordered well and I have a wikipedia tab open to make sure I don't miss any or including any that don't exist. »»

1906 I guess I'll add a scale type? major, minor, blah blah blah? I don't know. I think I clobbered something when I changed whichever that was to pitch. For the minors they all end in "m" and there's a chance I use that for parsing something or other or calculate what whole/semi tone pattern to follow. 

I guess that's just the scale. So yeah, I should add a property. 

1919 I have things in order and all the scales that are mentioned on wikipedia. Now I need to winnow down each one's list of valid notes and then I am probably done with this particular data set? maybe?

1936 key signatures all have notes arrays now. 

renamed whatever I had been calling the array of validn notes, probably notes, to pitches. consistant with A440 file that way.

1941 so I think I have the data files in place, at least well enough to start using them. 

1945 working on getting all the notes in the key to render. 

1951 not sure that this is a strong brain night. I have some okay logic, right now I"m just trying to get all the things into the stave. note. pitch, whatever I'm doing. 

1958 the notes in the cord are being rendered but still invalid ones and they are all bound to /2. 

I guess what I want to do is winnow down the list of things in the a440 file (programatically, filter down the data set) to only include notes that are valid. Then I can like, do all of them up then down or something like that. up at least because that's just how it will go. 

So filter out the notes (from a440) that cannot be played by the chosen instrument which for now is always bass. 

so bass has a range from E1 to G4. 

2005 oh I haven't named all the notes in the a440 file. It's not ideal I guess oh I guess I can get indexof the low range and index of the high range and get the slice of the array between those two points. 

2047 So.. 

2052 built logic to find the high and low index but still haven't named all the notes, so doing that now. 

20240114 0948 low and high indexes are populating correctly. I struggled to stay awake last night while editing the file and was impressed this morning when I saw I'd made it through the first 6 octaves so it was a quick wrap up and just now confirmed in the logs that it is working properly. so I need to carve out the slice of the array.

0953 I'm trying a new editor, Cursor, which has AI integration. 

0956 There's a hotkey to get to the ai chat which is good, but I can't figure out the hotkey to get back to the editor window. 

0958 oh shit, ace, just hitting ESC goes back. Alright, this is going to work nicely at least to start. We'll see how quick I burn through 200 queries in a month I guess. But first: Shovelling!

1109 so I have the list of notes trimmed to those playable by bass, now I need it to be edited just for those notes in the thing. So add a filter pass.. 

1152 I need to get the key signature into the practice output, otherwise it all just looks like notes. Or accidentals, which I'd rather not do I think, get that key signature in there. 

1157 lunch, then an outing to remove some clutter. Upon return, I want to add a "vexflow()" method or somesuch to KeySignature (the class, not the datafile) which will return the `pitches[0] + isMinor ?: 'm':''` or something along those lines, because vexflow needs "C" and "Cm" to get c major and minor key signatures. Right now I'm just outputing the pitches[0] which gives the root note but will always be major scales because it's not adding the m. 

1459 
1502 sitting with an apple, the fruit and also the computer but that's not funny. So figuring out key signature.

1506 so I want to wrap the raw data in the KeySignature class and I need to slightly rewrite the thing to do it.

1511 constructing it, need the method that returns the vexflowKeySignature? or something like that. 

1513 now I need to use it in the page. 

1524 I am not sure why I am unable to add the key signatures. I have done it in this project but it is not working here. So look at the thing in this project, FirstBar. 

1525 I am thinking I should start looking into VexFlow or like whatever their simplified tool is. I don't need this level of complexity I think I am working at too low a level. I'm going to try to get the key signature working because I don't want to rewrite into all that stuff but I mean if I have to I will. And it might be best. So maybe I should just spend time on it now. 

1526 EasyScore, that's what they call it. 

> Here's an example of an EasyScore line representing a C major chord (quarter note) followed by four eighth notes: (C4 E4 G4)/q, D4/8, E4, F4, G4.

1528 I just had an interface-y thought. Perhaps it is possible to show multiple instruments on one screen. I don't know that's probably a bad idea as I consider it further like if I showed every note on the piano that could be played and then highlight the bass ones in a different colour... ? That's interesting possibly but is there utility in it? I'm genuinely unsure. 

1530 I read through https://github.com/0xfe/vexflow/wiki/Using-EasyScore and I think I do want to do it. So when I render all a bass guitar's playable notes it goes way up into the treble zone but when I only have the bass cleff it's like super tall and stuff and I don't know. Maybe it is what I want, bass clef only? 

I don't want to have to worry about stem direction but I do think I have to but it's kind of irritating that it is stem direction for an entire set of notes so can they go up and down in the same run? 

1533 it isn't explicit but in the first demo there are notes with downward and upward pointing stems and nothing in the example code talks about stems. So yes, this is the right direction. Should commit though before I go ripping this stuff apart.

1536 pushed it up without the key signatures, so I can give that a noodle figure out what notes are what fretstrings etc.

so try this easy score stuff.

1539 task switch time. Started setting up the EasyScore stuff, haven't imported anything for it yet though. So like, fresh start. 

1904 I have the thing showing up. but not with the notes I want yet, just the example stuff. I need to wire it all together, yet. 

1914 slight difference in how core vexflow and this other one describe themselves. core is "G/2" ? maybe? I might be wildly wrong about that? andyway I'm removing that / programmatically but it might be best to take it out of the data entirely if I'm doing something weird here. Which I might be? But it seemed to parse it correctly so what do I know. 

Anyway, now the problem is too many ticks. So I need to break things out into one measure of notes, so chunks of 4 I suppose, do them as quarter notes across several measures. More dishses first though. 

2106 it is a little frustrating, I'd like to just give it an array of notes but I guess I'm working on wrapping that a bit or something. 

2108 so maybe I can update section or I'll make "EasyBar". 

20240116 2103 Trying a brown dark theme. It's creamy? I don't know, it's .. maybe too much. I'm more a light theme person, in general. 

Trying it though. 

So I made an easy bar class, I don't really know where I left - drifted - off. 

2107 So I have EasyBar but it renders the whole thing right now, not just a single bar. And I'm trying to wrap it up in like.. whatever. css grid. but it all gets dumped into one cell, so it's giant and vertical. So I need to move the break-it-into-4s logic into .. page? 

No, another score I guess? 

2110 oh most of that logic is in +page anyway probably? 

2121 I think it all keeps veing added to the same container? 

2122 I found the first bar content, it's in the pile too. So.. the render is behaving differently because it has an id that gets reused in all of them, so make it have the index.

2125 yeah, adding the index made a difference. 

20240117 2100 Short bit tonight. So layout stuff...

2130 made a bit of progress, first bar shows now. need to work on sizing and padding. 

20240118 2147 late start, not expecting much but sizing and padding tweaking doesn't need much.

2155 okay so ugh, this layout stuff is irksome. So what is happening right now is that the incidentals (flats or sharps) Are being shown for the notes on the not-first bars, presumably because they don't have the key signature on them. 

Anyway, the way around this is to just always show the incidentals. don't show the keys. I am considering not showing the clefs but that seems like a bad idea and this seems like a cop-out. 

20240121 0954 back in vs code. I'm not sure about cursor. we'll see. 

So I'm trying to help with practicing sight reading. 

0957 not fully in yet. But the goal of what I'm working on now, and broadly all of this, is as a practice tool for me and what I want to do is learn the bass, especially the fret board. I also want to learn music theory, and the two are intertwined. 

So for right now, it is fine not to get the layout stuff right. I guess probably the thing I need to do is build out a layer on top of vexflow that is like easy score but does my shit my way? Or use easy score for some and dip down into the lower layers but I guess what I will want to do first is .. brain clear. I just want to get this to work without having to dip down into lower layers, so I will do it without the key and time signature. 

1003 maybe I can put a bunch of stuff into the note instance or whatever. What clef to draw it one, whether or not to render stem up or down by default. Probably if there are chords or multiple voices and stuff that will get more complex but the default case is going to cover a lot of ground. 

So right now what I'm seeing - and this might be an easy score thing which, grumble fucker if it is - each measure has the end line and they don't take up the full space of the div even though their dimensions are supposed to be the same? I guess bar lines exit so having them render is fine I just need to get them using all the space. 

1039 I really have no reasonable guesses as to why these things aren't filling in the bars. 

So, alright, maybe it is time to stop messing around with easy score. This ain't easy and for me it's harder. Or something. Anyway, I went to easy score because why? Maybe because I was struggling to sequence the notes correctly but I think some of that logic got worked out to get easy score going. So yeah, flipping back I suppose. 

1041 so I just changed which render function I'm using, or so I thought, and nothing changed which seems very wrong. 

1044 I'm messing around in the render stuff in page but that doesn't get used any more I guess probably it's all in EasyBar.

1045 so maybe I should fuck around with easyflow in here a little bit. After cleaning up page. 

1048 so changing the width in this one didn't seem to do much either
1049 yeah, no change if I specify width when adding the stave or setting x. 

1054 okay so I'm in the middle of trying to get easy score working by creating a new sinstance of SystemStave and setting it up but I don't know where it is to import it from. 

It might be worth rolling back my changes and walking through the steps again when I sit down, especially if I can't see where I was going I may have really confused the issue because I accidently got into trying to instantiate stave.

Anyway, I think there are ways to set stave width in easy score and I'm close to it, but interrupted. Read above for more. 

1244 so work on this for a bit, then when Jen is done making sauce I'll do a pass on the kitchen and make myself something that biases towards healthy to consume.

1245 so there really isn't much to rollback, I just need to make the treble stave in an easyscore way.

1254 I am making small progress, actually. If I do not specify a width then weird stuff doesn't happen. 

1259 I'm swinging back around to not using easyscore. There's assumptions that it makes that I don't want made, I think? Or that I just don't udnerstand, but if I draw just the firstbar (which is the current state of the app) it's actually like 500 pixels wide with no width boundaries or anything like that given to it but even then it doesn't fill the container it is in it's just like, there. So yeah, I think that I can make another Bar class that builds up step by step again - best way for me to learn things, I often think. 

Anyway, kitchen time for a while again. Upon return, a new Bar class! PracticeBar perhaps, at least to start. Good name in a couple of ways.

1506 now I'm tempted to try to pass the renderer down to the bars and draw everything on one big score instead of with the divs and stuff. But that's definitely not the way this thing expects it to be, I think? 

1510 there are not a lot of options to Vexflow so just keep learning at it I suppose. 

I'm going to keep going with what I have I guess.

1511 finally commented out everything except the first bar. Stick to the small stuff. 

20240206 1924 I don't really remember what I was in the middle of with more detail than "getting things rendering the way I want". It is a difficult wall to penetrate. I can't remember at all what I was doing. I guess load the page and see what's what.

1928 No errors in the console. The full - oh I think I am starting to remember. The height is being frustrating. I mean, it's explicitly visible right now, the bass stave is cut off but the div is higher and stuff. So I'm constraining heigher or something somewhere?

1929 working in practicebar

1930 there isn't anything in this component that looks like it is explicitly constraining things. 
1931 the div that is practiceBar has a height of 500. so does easyBarContainer (I guess I"m still using easybar? it is so close to working I guess)

The svg is height 160 and if I edit it directly to increase the height more of it shows. Will probably be better as 250. so what's setting something to 160? 

1932 nothing is setting height to 160 but a lot of things are setting _width_ to 160. Foolish mortal. 

yeah the svg is plenty wide, probably 500..

1934 alright, well, stepping away for a few weeks or whatever pays off again. 

1935 so show more things again. 

1936 Okay so in the not-first bars I have a hardcoded sequence of notes, probably from the test I grabbed the initial code from. And that's fine because rendering the stuff I want to do programatically is the next problem, right now the thing to focus on is on how all the bars come together in the grid, which they are not doing nicely. 

1939 okay so there's only the few bars. One is rendering entirely empty though, so what's up there? 

1943 is it the same like are they overlapping? I don't think so, because usually in that situation the notes would be visible. But thats if they're rendering on each other not overlapping but the inspector element selector thing highlights distinct areas

1944 this is weird. 

1945 it's the index. the index needs to be index + 1. 
1957 alright, the score is rendering pretty well. I did have to fix the width. OOhhh first bar should be smaller and indented. 

1959 yessssssss, I like this. okay!

fake notes still. 

2003 

2007 I am rendering notes on the bass clef now, previously it was just the treble clef. However, the notes are rendering in the exact same points on the bass clef as they do on the treble clef, so like the note is a c and on the treble clef it's rendering as a floating-below note with a line through it. Middle C, right? So but the note in the bass cleff which is also a C is rendering visually the same but that would be the wrong point on the staff.

For one thing it should be a middle c so it goes above but that's a me sorting out location sort of thing, but it should like, be in an actual c position in the bass staff too. 

2011 the example of setting notes up and stuff in the easy score docs looks much easier. Am I actually using easyscore? Easyscore ends up in the class names and stuff so .. probably? 

2012 yeah mine does some kind of "addTickables" instead of like a simple array of notes so I'll try to change.

2012 oh yeah, okay, I'm definitely not using easyscore. I'm not sure why that class is showing up? "easyscore" only shows up in this document. 

2014 oh! "c/4" isn't "a c quarter note" it is "c4". middle c. The note instances have a vexflow property that outputs in this fashion. That's handy, thanks Past Rob!

2017 I'm just going to hardcode in the notes collection thing which stave they should use. 

2032 Alright I think it's time to tie a bow and step away, at least for a while. Do it cleanly.

So I'm working on the /keys page which picks a key at random and renders all the playable notes on a bass on the score. Eventually more flexibility about instrument or whatever but right now that's the goal, for me to practice with. 

but the rendering is bad. I'm working through how to split the notes across the clefs and I guess it's not going super great right off the cuff. 

20240207 1855 tidy time soon so just dipping in, maybe get my brain going. 

1857 oh, my timer went off a few minutes _before_ 7. pout. So in this brief moment I updated +page to only render the first measure. There are no treble notes and there is nothing complaining about Voices missing Notes and such like. But the notes are rendering much too low. I guess for bass I'm supposed to render an octave higher, right? But my translation to bass logic should have handled that? Or something? Whatever sets up the measures. Right? Maybe. 

Anyway, dishes aren't going to do themselves. 

1944 I'm not sure I have a lot in me but perhaps a bit. 

1947 oh it isn't going to work like this. Huh this is .. confusing to me. 

So if I have it right I'm getting the voices doesn't have enough notes in situations where the notes in the measure cross from bass to treble clefs. So if I set the single measure being rendered to measures[1], bassNotes gets three entries and trebleNotes gets four. 

This is a problem, because each needs to have 4 (or zero). But I don't want to put like, rests in. Maybe there are transparent notes?

I need to look at the test/examples.

1949 Looking at the score on the vexflow.com front page I realize that yes I probably do need to put a rest in for all the places in each clef where nothing is being played. 

I'll keep looking deeper at the tests though or perhaps there are some complete score examples somewhere.

1953 Okay, so yes I need to do that. Each voice needs to have something to say or something to not say. 

Also I found this: https://github.com/opensheetmusicdisplay/opensheetmusicdisplay looking for things that use vexflow. It maps MusicXML to VexFlow. It might be the case that MusicXML is easier to use. 

Next time I get frustrated with vexflow, perhaps I'll remember to look for this. 

2000 clumsy solution for now, just adding a quarter note to the opposite stave. 

2003 so it isn't treating the bass cleff as a bass clef. It renders c/4 in the spot where it would if it was a treble clef, one line below the staff. Not as though it were a bass clef. Frustrating.

2004 I _did_ outright copy the treble stave stuff so maybe I missed something obvious and just setting the clef is not enough.... ? so weird.

2006 Nothing I can see in the Stave class other than setClef that seems like it might be important. To the tests, I suppose?

2011 oh, the clef needs to be set explicitly in the note. 

2054 one snacktime later...

So it looks good, actually, I think. 

I can probably tighten up the bass clef a bit actually. 

but I want to get it deployed soon too.

so don't fiddle too much.

2057 okay, going to leave it for now actually, get this bit live, then fiddle. because I"ll need to adjust the stems to make it look right.

2101 oh and there's no key signatures. Latest is live though, it's a good start.

but I think key signatures are actually going to be really important so figure that out before the other stuff.

2114 kk, key signatures are in.

2153 root notes are highlighted. There's some cases - I think it's all sharp keys, but at minimum C# - where the root note is missed because it isn't right. 

2154 oh the datafile had the wrong root note value. 

2215 the pitches are weird for key "Cb major"

2222 I feel like this has been weird before. Because it has a Bb so a Cb is just a B? but it it doesn't play a B either it just from Bb to Db. And that's not right. 

20240208 1853 another short pop-in before kitchen tidy time. 

1853 I don't know if there's still a problem with Cb minor, it hasn't turned up in a couple dozen refreshes. It will eventually though probably, and I might even notice, if it is still not working correctly. 

I guess the next thing I'd like to do is merging rests? Or something about stems. In theory stem direction could be associated with the note perhaps. That's a fairly straight-forward task if true but merge rests is more interesting. 

Could do something about successive rests, maybe? 

I'm not sure how to tackle it. 

[note, note, note, rest] - no change
[note, note, rest, rest] - [note, note, halfrest]
[note, rest, rest, rest] - [note, rest, halfrest]
[rest, rest, rest, rest] - [fullrest]

1858 obv that's not the only combinations possible just some examples to work through. 

So take the third one, [n,r,r,r]. I want to reduce that to [n, r, hr] but I'm not sure that .reduce is the mechanism. 

1859 so think about it in a loop, if I go through each - I guess I can make like a processRests function to make it a bit less heavy in this code. 

1903 Okay, so I'm going to do kitchen things now. I made the function and it is running and it branches with regards to a note or a rest so now I can like.. look at things. I want to keep the previous note around probably so I can be like, is this note and the previous note a rest? 

but I also might want to switch to a normal for loop so I have the index and I can like, go up through any previous ones because if I convert something to a half and the previous was a half, I need to convert the previous to a whole instead. same for prev == q && curr == q. I guess I have to - well, the way this is built it will always be quarter notes that need merging. So at least that will be easy. Either there's a quarter or a half ready to go. Anyway. Kitchen. 

2009 been avoiding looking at this again but I guess I want to chip away a bit. 

2025 Alright, things are a mess. 

20240209 1940 So, made a big mess, did I? The logic for the rest blending is bad, that much I remember. 

1941 dropped down to one bar to work with. ultimately treble notes should compress to 1 whole note.

1950 so the logic is reasonably good I think now. Ugly but like, should be functional. Except that the array isn't like, looking right? 

1951 restedNotes is always length 0 but also it like, has notes in it? I can't explain what I'm seeing in the console. The array is displayed as `[ {...} ]` 

2000 

> The push() method of Array instances adds the specified elements to the end of an array and returns the new length of the array. 

2002 so am I like, pushing the entire notes array in even though I'm pretty sure I'm just putting the one element in? Something is fuck. ee.

2005 if I push an empty object onto the array it still gets 4 elements. If I don't do any push at all, it is fine. 

this is messed up, I don't understand. 

2007 maybe drawnotes is changing it. yep, that's probably what's happening, and the console is showing it _after_ drawnotes is done with it. 

so drawnotes is adding in rests for the end of bar stuff. 

2008 so maybe process rests from inside drawnotes, after it adds thes tuff.

2014 some slight progress. inching my way along. Not entirely baffling but not coming together as quickly as one might hope. 

2014 oh, hmm, okay. I'm comparing against the notes array, but it is restedNotes where I'm putting the rests. So previousNote will never be a half. So. Alright. I'm looking at the wrong things then. 

So if it's the first note it is a rest, if the previous wasn't a note it is a rest etc but that previous is the problem. Previous is the rested note. 

should be. 

2019 getting closer! Showing a half and two rests in the first bar now. 

2038 I'm trying to get flashcard mode to work. I shouldn't bother, there's not a ton of value in it? But also this should be fucking easy but reactivity eludes me. 

So next time I get into this, I should decide if I want to keep flashcard mode or leave it alone. 

I do want a flashcard tool though so.. maybe? 

20240210 1410 So I'll comment it out and do something with it another time, perhaps. The thing I do want to make progress on next is a randomly generated exercise in the "pick a progression" vein. 

Do I? Or maybe just get things tidy and deploy and then see what happens as I use it. That latter one might be the better approach. Because I'm not sure I have more of this in me right now. 

20240426 2132 I want the randomly generated exercise, still. Sight reading practice and fretboard exploration. 

20240502 2116 Am out of habit on the personal projects and slowly getting ready to get back in.  So, the 

2117 where is the logic? right, in `key/+page.svelte`

2118 so 

2123 I think I should make a new route. I can probably make use of PracticeBar and such like, just copy the file over as the starting point probably and then rework what 

2128 okay so I copied it and ripped out the flashcard stuff and so it loads, still doing the "all the playable notes" thing. 

For this one it will be a progression of some sort. I wrote logic somewhere I'll get to it eventually but it's like, pick a number pattern and work through it a few times or something. 

So yeah I should read that again and warm it back up. I also want to add in the note name underneath it. No tab, but that hint will make a big deal. With the number, so like C4 or Bb2 or whatever. 

So yeah the thing I need to do is generate `measure`

2133 So what resources do I have for me? Pick the key but then also a .. no it isn't patterns, just randomly choose 3-4 numbers? 

1, 2, 6
1, 3, 5, 8

Yeah I'm good x


2141 so how do I pick those numbers? random 

2151 fumbling with logic. getting there but doing a change. Instead of picking the count I'll just pick elements from the array until it's done so maybe I get 1,8 or maybe I get 1,2,3,4,5,6,7,8?

hmmm. maybe not. 

so if count is 3, then I can pick 1, then any number between 2 and 7, then any number between n-1 and 8.

and if it is 4, same thing but the upper boundary at first is 6, not 7. 